import React, { useState, useEffect, useRef } from 'react';
import { Toast } from 'primereact/toast'; // Toast de prime react : https://primereact.org/toast/
import { InputText } from "primereact/inputtext";
import '../assets/css/Home.css';

export default function TodoList() {
  const [taskInput, setTaskInput] = useState(''); // déclare un état local pour l'entrée des tâches
  const [tasks, setTasks] = useState(() => {
    const storedTasks = localStorage.getItem('tasks'); // au chargement de la page, on récupère les tâches présentes dans le LocalStorage s'il y en a
    return storedTasks ? JSON.parse(storedTasks) : [];
  });
  const [showDeleteToast, setShowDeleteToast] = useState(false); // déclare un état local ppour le toast de suppression
  const toast = useRef(null);

  // enregistre chaque nouvelles tâches dans le LocalStorage et affiche le toast
  useEffect(() => {
    localStorage.setItem('tasks', JSON.stringify(tasks));
    if (showDeleteToast && toast.current) {
      toast.current.show({
        severity: 'info',
        summary: 'Tâche terminée',
        detail: `Bon travail !`,
        life: 3000 
      });
      setShowDeleteToast(false); // réinitialise l'état pour ne pas afficher le toast de suppression à nouveau
    }
  }, [tasks, showDeleteToast]);

  // ajoute la tâche dans la liste et déclenche le toast
  const addTask = () => {
    if (taskInput.trim() !== '') { // on vérifie si l'entrée n'est pas vide
      setTasks([...tasks, taskInput]); // ceci ajoute la tâche à la liste
      setTaskInput(''); // réinitialise  l'entrée afin que ça soit vide
      if (toast.current) {
        toast.current.show({
          severity: 'success',
          summary: 'Tâche ajoutée',
          detail: 'La tâche a été ajoutée avec succès.',
          life: 3000
        });
      }
    }
  };

  // permet de supprimer une tache de la liste et déclenche le toast
  const deleteTask = (index) => {
    const updatedTasks = tasks.filter((_, i) => i !== index); // créé une liste de tâche sans celle à l'index indiqué
    setTasks(updatedTasks); // met à jour la nouvelle liste de tâches
    setShowDeleteToast(true); // Déclenche l'affichage du toast de suppression
  };

  return (
    <div className="content">
      <h1>To-Do List</h1>
      <Toast ref={toast} />
      <div className="flex justify-content-center align-items-center gap-4">
        <InputText
          className="input-task"
          type="text"
          value={taskInput}
          onChange={(e) => setTaskInput(e.target.value)}
          placeholder="Ajouter une tâche"
        />
        <i className="add-task pi pi-plus" onClick={addTask}></i>{/* icons provenant de la  bibliothèque UI PrimeReact */}
      </div>
      <ul className="list-task">
        {tasks.map((task, index) => (
          <li key={index}>
            <div className="task">
              {task}
              <div className="flex gap-3">
                <i
                  className="done-task pi pi-check"
                  onClick={() => deleteTask(index)}
                ></i>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
