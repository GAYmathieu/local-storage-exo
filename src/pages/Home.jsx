import React from 'react'
import TodoList from '../components/ToDoList'

export default function Home() {
  return (
    <div className="flex justify-content-center">
      <TodoList/>
    </div>
  )
}
